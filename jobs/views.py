from django.shortcuts import render,render_to_response
from django.http import HttpResponse
import requests
from BeautifulSoup import BeautifulSoup
# Create your views here.


def jobs_base_view(request):
    print "Hi"
    return render_to_response('jobs/basepage.html')


def next_page(request):
        r = requests.get('https://api.adzuna.com:443/v1/api/jobs/in/search/1?app_id=22373d9e&app_key=ca2ef7296a6aeb12979eed04ff60ee80&results_per_page=10&what=%22skill%22&location0=India&location1=Maharashtra&location2=Mumbai')
        vvv = r.json()
        json = vvv['count']
        if  int(json)<=50:
            pages = 1
        elif int(json)%50 == 0:
             pages = int(json)/50
        elif int(json)%50 !=0:
            pages = int(json)/50
            pages = pages + 1
        return pages


def pagination(request,get_pages):
    for i in xrange(1,get_pages + 1):
        yield i

def get_jobs(request,page_no):

    r = requests.get('https://api.adzuna.com:443/v1/api/jobs/in/search/'+str(page_no)+'?app_id=22373d9e&app_key=ca2ef7296a6aeb12979eed04ff60ee80&results_per_page=10&what=%22skill%22&location0=India&location1=Maharashtra&location2=Mumbai')

    if r.status_code == 200:
         vvv = r.json()
         zzz = vvv["results"]
         list_of_results = []
         for i in zzz:
                try:
                    redirect_url = i["redirect_url"]
                except:
                    redirect_url = "NOT AVAILABLE"

                try:
                    title = i["title"]
                    title = BeautifulSoup(title).text
                except:
                    title = "NOT AVAILABLE"

                try:
                    salary_min = i["salary_min"]
                except:
                    salary_min = "NOT AVAILABLE"

                try:
                    salary_is_predicted = i["salary_is_predicted"]
                except:
                    salary_is_predicted = "NOT AVAILABLE"

                try:
                    company = i["company"]["display_name"]
                except:
                    company = "NOT AVAILABLE"

                try:
                    contract_time = i["contract_time"]
                except:
                    contract_time = "NOT AVAILABLE"

                try:
                    salary_max = i["salary_max"]
                except:
                    salary_max = "NOT AVAILABLE"

                try:
                    description = i["description"]
                    soup = BeautifulSoup(description)
                    description = soup.text
                except:
                    description = "NOT AVAILABLE"


                try:
                    date_ = i["created"].split("T")[0]
                except:
                    date_ = "NOT AVAILABLE"

                body = {"redirect_url":redirect_url,
                        "title":title,
                        "salary_min":salary_min,
                        "salary_is_predicted":salary_is_predicted,
                        "company":company,
                        "contract_time":contract_time,
                        "salary_max":salary_max,
                        "description":description,
                        "date_":date_
                        }
                list_of_results.append(body)
         return list_of_results





def view_jobs(request):
    get_pages = next_page(request)
    get_pagination = pagination(request,get_pages)
    get_pagination = get_pagination.next()
    get_jobs_function = get_jobs(request,get_pagination)
    next_ = get_pagination + 1
    prev = get_pagination - 1
    print get_jobs_function
    return render_to_response('jobs/about.html',{'body': get_jobs_function,'prev':prev,'next':next_})





def next_jobs(request,pno):
    r = requests.get('https://api.adzuna.com:443/v1/api/jobs/in/search/'+str(pno)+'?app_id=22373d9e&app_key=ca2ef7296a6aeb12979eed04ff60ee80&results_per_page=10&what=%22skill%22&location0=India&location1=Maharashtra&location2=Mumbai')

    if r.status_code == 200:
         vvv = r.json()
         zzz = vvv["results"]
         list_of_results = []
         for i in zzz:
                try:
                    redirect_url = i["redirect_url"]
                except:
                    redirect_url = "NOT AVAILABLE"

                try:
                    title = i["title"]
                    title = BeautifulSoup(title).text
                except:
                    title = "NOT AVAILABLE"

                try:
                    salary_min = i["salary_min"]
                except:
                    salary_min = "NOT AVAILABLE"

                try:
                    salary_is_predicted = i["salary_is_predicted"]
                except:
                    salary_is_predicted = "NOT AVAILABLE"

                try:
                    company = i["company"]["display_name"]
                except:
                    company = "NOT AVAILABLE"

                try:
                    contract_time = i["contract_time"]
                except:
                    contract_time = "NOT AVAILABLE"

                try:
                    salary_max = i["salary_max"]
                except:
                    salary_max = "NOT AVAILABLE"

                try:
                    description = i["description"]
                    soup = BeautifulSoup(description)
                    description = soup.text
                except:
                    description = "NOT AVAILABLE"


                try:
                    date_ = i["created"].split("T")[0]
                except:
                    date_ = "NOT AVAILABLE"

                body = {"redirect_url":redirect_url,
                        "title":title,
                        "salary_min":salary_min,
                        "salary_is_predicted":salary_is_predicted,
                        "company":company,
                        "contract_time":contract_time,
                        "salary_max":salary_max,
                        "description":description,
                        "date_":date_
                        }
                list_of_results.append(body)
                # next_ = pno + 1
                # prev = pno - 1
         print list_of_results
         return render_to_response('jobs/nextpages.html',{'body': list_of_results})

   #return HttpResponse ,"prev statement"

def prev_jobs(request,pno):
    print pno
    return render_to_response('jobs/basepage.html')