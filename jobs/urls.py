from django.conf.urls import url
from .views import jobs_base_view,view_jobs,next_jobs,prev_jobs




urlpatterns = [
    url(r'^$', jobs_base_view),
    url(r'^viewjobs/$', view_jobs),
    url(r'next/(\d+)/$', next_jobs),
    url(r'prev/(\d+)/$', prev_jobs)
]
