from django.conf.urls import url
from .views import startpage,contactpage,thanks,about,register




urlpatterns = [
    url(r'^$', startpage),
    url(r'^register/$',register),
    url(r'^contact/$', contactpage),
    url(r'^about/$', about),
    url(r'^contact/thanks/$', thanks),

]
