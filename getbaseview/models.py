
from django.db import models

# Create your models here.


class submitdetails(models.Model):
    firstname = models.CharField(max_length = 500)
    lastname = models.CharField(max_length = 200)
    email = models.CharField(max_length = 20000)
    phone = models.CharField(max_length = 500)
    message = models.CharField(max_length=500)
    def __str__(self):
        return self.email

