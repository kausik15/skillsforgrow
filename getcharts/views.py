from .models import ListModel
from django.db.models import Q
from django.shortcuts import render,render_to_response,HttpResponse,HttpResponseRedirect
import json




def search_charts(request):
    return render_to_response('getcharts/searchcharts.html')


def get_charts(request):
    try:
        query = request.GET.get('q')
        print query
        query_list = ListModel.objects.filter(Q(skill_name__istartswith=query))
        render_list = [['Year', 'Average Salaries']]

        for i in query_list:
            for j in i.test_list:
                  render_list.append ([j[0].encode('ascii'),j[1]])
        # {'djangodict': json.dumps(render_list),"skill":str(i.skill_name)}
        return render(request,  'getcharts/chartspage.html', {'djangodict': json.dumps(render_list),"skill":str(i.skill_name)})
    except:
        return render(request,'getcharts/chartnotfound.html',{"skill":str(query)})


