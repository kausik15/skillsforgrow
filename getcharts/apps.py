from django.apps import AppConfig


class GetchartsConfig(AppConfig):
    name = 'getcharts'
