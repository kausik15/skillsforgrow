from django.contrib import admin


from .models import ListModel

# Register your models here.


class ContactFormAdmin(admin.ModelAdmin):
    list_display = []
    class Meta:
        model = ListModel


admin.site.register(ListModel)
