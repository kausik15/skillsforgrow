from django.conf.urls import url

from .views import get_charts,search_charts


urlpatterns = [

    url(r'^$', search_charts),
    url(r'^charts/$', get_charts),


    ]