from django.conf.urls import url
from .views import get_base_page,getcourses




urlpatterns = [
    url(r'^$', get_base_page),
    # url(r'^getcourses/$', getcourses),
    url(r'^results/$', getcourses),
]
