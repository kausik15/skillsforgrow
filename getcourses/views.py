from django.shortcuts import render,HttpResponse,render_to_response

from .models import coursedatabase
from django.db.models import Q

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



def get_base_page(request):
    return render_to_response('getcourses/getcoursepage.html')


def getcourses(request):


    query = request.GET.get('q')
    query_list = coursedatabase.objects.filter(Q(coursename__istartswith=query)).order_by('coursename')
    paginator = Paginator(query_list, 5) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        results = paginator.page(1)
        #return render(request,  'visualize/results.html', {'contacts': "query not found in database"})
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        results = paginator.page(paginator.num_pages)

    return render(request,  'getcourses/searchcoursepage.html', {'contacts': results})
