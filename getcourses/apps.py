from django.apps import AppConfig


class GetcoursesConfig(AppConfig):
    name = 'getcourses'
