
from django.db import models

# Create your models here.



class coursedatabase(models.Model):
    courseurl = models.CharField(max_length = 500)
    coursename = models.CharField(max_length = 200)
    coursedetails = models.CharField(max_length = 20000)
    courseimageurl = models.CharField(max_length = 500)
    coursesourcewebsite = models.CharField(max_length=500)
    coursetargetaudience = models.CharField(max_length = 100000)

    def __str__(self):
        return self.coursename
