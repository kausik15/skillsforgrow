
from django.conf.urls import url,include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/', include("getbaseview.urls")),
    url(r'^home/getcourses/', include("getcourses.urls")),
    url(r'^home/getcharts/', include("getcharts.urls")),
    url(r'^home/jobs/', include("jobs.urls")),
    url(r'^home/login_registration/', include("login_registration.urls"))


]
